<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Projects;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    private $u;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function redirectNoUser()   //__contstruct() has the same problem
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionError()
    {
        $this->redirectNoUser();

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->redirectNoUser();
        $users = User::find()->all();
        $pr = Projects::find()->all();
        $titles = array('Id','Name','Title','Cost','Date start','Date end');
        $href = array('href' => '/site/user', 'title' => 'Add user');
        $script = 'scriptProj';
        return $this->render('index',array('user' => $users, 'proj' => $pr, 'items' => $titles, 'hrefs' =>$href ));
    }
    public function actionUser(){
        $this->redirectNoUser();
        $user = User::find()->all();
        $titles = array('Id','Fio','Login','Password');
        $href = array('href' => '/', 'title' => 'Add proj');
        $script = 'scriptProj';
        return $this->render('user',array('user' => $user, 'items' => $titles, 'hrefs' =>$href ));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        } 
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $this->redirectNoUser();

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $this->redirectNoUser();

        return $this->render('about');
    }
    private function isAjax(){
        $this->redirectNoUser();

    }
    public function actionEditfield()
    {
        $this->redirectNoUser();
        $id = Yii::$app->request->post('id');
        $pr = Projects::findOne($id);
        $this->u = new Projects;
        $this->changeUserData(Yii::$app->request->post());
        $this->u->save();
    }
    public function actionCreatefield()
    {
        $this->redirectNoUser();
        $this->u = new Projects;
        $this->changeUserData(Yii::$app->request->post());
        if($this->u->save()){
            $user = User::find()->all();
            return $this->renderPartial('tr', array('p'=>$this->u, 'user' => $user));
        }
    }
    public function actionDeletefield()
    {
        $this->redirectNoUser();
        $id = Yii::$app->request->post('id');
        $pr = Projects::findOne($id);
        $pr->delete();
    }
    public function actionEdituser()
    {
        $this->redirectNoUser();
        $id = Yii::$app->request->post('id');
        $this->u = User::findOne($id);
        $this->changeUserData(Yii::$app->request->post());
        $this->u->save();
    }
    public function actionCreateuser()
    {
        $this->redirectNoUser();
        $this->u = new User;
        $this->changeUserData(Yii::$app->request->post());
        if($this->u->save()){
            return $this->renderPartial('tr_user', array('u'=>$this->u));
        }
    }
    private function changeUserData($post){
        foreach($post as $k => $v){
            $this->u->{$k}= $v;
        }
    }
}
