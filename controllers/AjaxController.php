<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\User;
use app\models\Projects;

class AjaxController extends Controller
{
    public function actionEditfield()
    {
        $id = Yii::$app->request->post('id');
        $pr = Projects::findOne($id);
        $pr->uid = Yii::$app->request->post('uid');
        $pr->title = Yii::$app->request->post('title');
        $pr->cost = Yii::$app->request->post('cost');
        $pr->date_start = Yii::$app->request->post('date_start');
        $pr->date_end = Yii::$app->request->post('date_end');
        $pr->save();
    }
    public function actionCreatefield()
    {
        $pr = new Projects;
        $pr->uid = Yii::$app->request->post('uid');
        $pr->title = Yii::$app->request->post('title');
        $pr->cost = Yii::$app->request->post('cost');
        $pr->date_start = Yii::$app->request->post('date_start');
        $pr->date_end = Yii::$app->request->post('date_end');
        if($pr->save()){
            $users = User::find()->all();
            return $this->render('tr', array('proj'=>$pr, 'user' => $user));
        }
    }
    public function actionDeletefield()
    {
        $id = Yii::$app->request->post('id');
        $pr = Projects::findOne($id);
        $pr->delete();
    }
}
