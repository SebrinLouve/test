<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m180809_231054_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'uid' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'cost' => $this->float()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date()->notNull(),
        ]);
        $this->addForeignKey(
            'fk-projects-uid',
            'projects',
            'uid',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        $this->dropTable('projects');
    }
}
