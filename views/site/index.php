<?php

/* @var $this yii\web\View */

$this->title = 'Welcome!';
?>

<table>
    <thead>
        <tr>
            <?foreach($items as $i){?>
                <td><?=$i?></td>
            <?}?>
            <td style='cursor:pointer; background-color: #0fbf0f;'><a href="<?=$hrefs['href']?>"><?=$hrefs['title']?></a></td>
        </tr>
    </thead>
    <tbody>
        <?foreach($proj as $p){?>
            <?= $this->render('tr', array('p'=>$p, 'user' => $user)); ?>
        <?}?>
        <tr id='insert'>
            <form id='form_create'>
                <td>New item</td>
                <td>
                    <select name="uid">
                        <?foreach($user as $u){?>
                            <option value="<?=$u->id?>"><?=$u->fio?></option>
                        <?}?>
                    </select>
                </td>
                <td><input type="text" name='title'></td>
                <td><input type="number" step="any" name='cost'></td>
                <td><input type="date" name='date_start'></td>
                <td><input type="date" name='date_end'></td>
                <td><div class='create btn'>Add item</div></td>
            </form>
        </tr>
    </tbody>
</table>


<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>

<script type="text/javascript">
$('.update').click(function() {
    var formData = $('#form_'+$(this).attr('field_id')).serialize();
    $.ajax({
        url: 'site/editfield',
        data: formData,
        type: 'POST',
        complete: function (data) {
            console.log(data.responseText);
        }
    });
})
$('.delete').click(function() {
    var id = $(this).attr('field_id');
    $.ajax({
        url: 'site/deletefield',
        data: {id : id},
        type: 'POST',
        complete: function (data) {
            $('#tr_'+id).remove();
        }
    });
})
$('.create').click(function() {
    var formData = $('#form_create').serialize();
    $.ajax({
        url: 'site/createfield',
        data: formData,
        type: 'POST',
        complete: function (data) {
            $('#insert').before(data.responseText);
            $("#form_create")[0].reset();
        }
    });
})
</script>

<style>
a{color: white;}
a:hover{color: white;}
table{
    width: 100%;
}
table select{
    width: 100%;
}
td{
    width: 14.2%;
}
thead td{
    text-align: center;
    border-right: 1px solid #ecf0f5;
    padding: 10px 0;
    background-color: #3c8dbc;
    color: #ecf0f5;
}
table .btn{
    text-align: center;
    padding: 10px 0;
    border-radius: 5px;
    transition: ease .1s all;
    width: 45%;
}
table .update{
    background-color: #3c8dbc;
    color: #ecf0f5;
}
table .delete{
    background-color: #222d32;
    color: #ecf0f5;
    float: right;
}
table .create{
    width: 100%;
    background-color: #3c8dbc;
    color: #ecf0f5;
}
.delete:hover{
    color: red;
}
.update:hover{
    background-color: #0fbf0f;
    color: #ecf0f5;
}
.create:hover{
    background-color: #0fbf0f;
    color: #ecf0f5;
}
tbody td{
    padding: 10px;
}
.id_field{
    width: 100%;
    background: transparent;
    border: none;
}
tbody tr:nth-child(2n){
    background-color: #e3e6ea;
}
</style>