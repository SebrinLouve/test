<?php

/* @var $this yii\web\View */

$this->title = 'Edit user';
?>

<table>
    <thead>
        <tr>
            <?foreach($items as $i){?>
                <td><?=$i?></td>
            <?}?>
            <td style='cursor:pointer; background-color: #0fbf0f;'><a href="<?=$hrefs['href']?>"><?=$hrefs['title']?></a></td>
        </tr>
    </thead>
    <tbody>
        <?foreach($user as $u){?>
            <?= $this->render('tr_user', array('u' => $u)); ?>
        <?}?>
        <tr id='insert'>
            <form id='form_create'>
                <td>New user</td>
                <td><input type="text" name='fio' ></td>
                <td><input type="login" name='login' ></td>
                <td><input type="password" name='password' ></td>
                <td><div class='create btn'>Add user</div></td>
            </form>
        </tr>
    </tbody>
</table>


<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js'></script>

<script type="text/javascript">
$('.update').click(function() {
    var formData = $('#form_'+$(this).attr('field_id')).serialize();
    $.ajax({
        url: '/site/edituser',
        data: formData,
        type: 'POST',
        complete: function (data) {
            console.log(data.responseText);
        }
    });
})
$('.create').click(function() {
    var formData = $('#form_create').serialize();
    $.ajax({
        url: '/site/createuser',
        data: formData,
        type: 'POST',
        complete: function (data) {
            $('#insert').before(data.responseText);
            $("#form_create")[0].reset();
        }
    });
})
</script>

<style>

a{color: white;}
a:hover{color: white;}
table{
    width: 100%;
}
table select{
    width: 100%;
}
table td{
    width: 20%;
}
thead td{
    text-align: center;
    border-right: 1px solid #ecf0f5;
    padding: 10px 0;
    background-color: #3c8dbc;
    color: #ecf0f5;
}
table .btn{
    text-align: center;
    padding: 10px 0;
    border-radius: 5px;
    transition: ease .1s all;
    width: 100%;
}
table .update{
    background-color: #3c8dbc;
    color: #ecf0f5;
}
table .create{
    width: 100%;
    background-color: #3c8dbc;
    color: #ecf0f5;
}
table .delete:hover{
    color: red;
}
table .update:hover{
    background-color: #0fbf0f;
    color: #ecf0f5;
}
.create:hover{
    background-color: #0fbf0f;
    color: #ecf0f5;
}
table tbody td{
    padding: 10px;
}
table .id_field{
    width: 100%;
    background: transparent;
    border: none;
}
tbody tr:nth-child(2n){
    background-color: #e3e6ea;
}
</style>